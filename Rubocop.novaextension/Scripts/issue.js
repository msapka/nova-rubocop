const generateIssue = (params) => {
  const {
    line,
    column,
    endColumn,
    severity,
    txt,
    source,
    copName
  } = params
    let issue = new Issue()

    issue.line = line
    issue.column = column
    issue.endColumn = endColumn
    issue.severity = severity
    issue.message = txt
    issue.source = source
    issue.code = copName

    return issue
}

module.exports = generateIssue