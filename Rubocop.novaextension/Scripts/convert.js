const generateIssue = require("issue")
const convertRubocopOffenceToKnownFormat = require('dataConvert')

const addOfencesToFile = (offences, uri, rubocopVersion, issueCollection) => {
  issueCollection.remove(uri)

  const issues = offences.map((offence) => {
    const params = convertRubocopOffenceToKnownFormat(offence, rubocopVersion)
    return generateIssue(params)
  })

  if(issues.length > 0) {
    issueCollection.set(uri, issues)
  }
}

const convertRubocopOutputIntoIssues = (output, issueCollection) => {
  const parsedOutput = JSON.parse(output)

  const files = parsedOutput.files
  const metadata = parsedOutput.metadata

  files.forEach((file) => {
    const uri = file.path
    const rubocopVersion = metadata.rubocop_version

    addOfencesToFile(file["offenses"], uri, rubocopVersion, issueCollection)
 })
}

module.exports = convertRubocopOutputIntoIssues