const rubocopSeverityToNovaSeverity = (rubocopSeverity) => {
  switch(rubocopSeverity) {
    case "fatal": return IssueSeverity.Error
    case "error": return IssueSeverity.Error
    case "warning": return IssueSeverity.Warning
    case "convention": return IssueSeverity.Hint
    case "convention": return IssueSeverity.Refactor
    default: return IssueSeverity.Info
  }
}

const convertRubocopOffenceToKnownFormat = (rubocopOffence, version) => {
  const endColumn = rubocopOffence.location.column + rubocopOffence.location.length

  return {
    line: rubocopOffence.location.line,
    column: rubocopOffence.location.column,
    endColumn,
    txt: rubocopOffence.message,
    copName: rubocopOffence.cop_name,
    severity: rubocopSeverityToNovaSeverity(rubocopOffence.severity),
    source: "Rubocop " + version
  }
}

module.exports = convertRubocopOffenceToKnownFormat