const runRubocop = (file, action, issueCollection, onSuccess) => {
  const runner = nova.config.get('msapka.Rubocop.command').split(" ").filter((i) => i !== "")

  let parameters = []
  if(action === "lint") {
    parameters = ["--format=json"]
  } else if (action === "autocorrect") {
    parameters = ["--format=json", "-a"]
  }
  
  let args = runner.concat(parameters)
  if(file) { args = args.concat([file]) }
  console.log(args)

  const process = new Process("/usr/bin/env", {
    args,
    cwd: nova.workspace.path,
    stdio: "pipe",
  })

  process.onStdout((output) => onSuccess(output, issueCollection))
  process.onStderr((err) =>  console.error(err))

  process.start()
}

module.exports = runRubocop