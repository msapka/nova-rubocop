const runRubocop = require('rubocopProcres')
const convertRubocopOutputIntoIssues = require("convert")

// single instance for the plugin
const issueCollection = new IssueCollection()

// Actions
const lint = (file, issueCollection) => {
  console.log("Running rspec lint on " + file)

  runRubocop(file, "lint", issueCollection, convertRubocopOutputIntoIssues)
}

const autocorrect = (uri, issueCollection) => {
  console.log("Running autocorect on " + uri)
  runRubocop(uri, "autocorrect", issueCollection, convertRubocopOutputIntoIssues)
}

// Commands
nova.commands.register("lintCurrentFile", (editor) => {
  lint(editor.document.path, issueCollection)
})

nova.commands.register("lintProject", (editor) => {
  lint(undefined, issueCollection)
})

nova.commands.register("autoCorrectCurrentFile", (editor) => {
  autocorrect(editor.document.path, issueCollection)
})

nova.commands.register("autoCorrectCurrentProject", (editor) => {
  autocorrect(undefined, issueCollection)
})

//Listeners
nova.workspace.onDidAddTextEditor((editor) => {
  if(editor.document.syntax === "ruby")  {
    lint(editor.document.path, issueCollection)
  }

  editor.onDidSave((editor) => {
    if(editor.document.syntax !== "ruby")  { return }
    if(!nova.config.get('msapka.Rubocop.runOnSave')) { return }

    lint(editor.document.path, issueCollection)
  })
})



